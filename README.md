# Terrain Settings - Unity 3D #

A Unity permite configurar parâmetros de terreno de forma global. Permitindo um controle entre a qualidade gráfica e o desempenho da simulação. Tendo um terreno em cena, você pode acessar, no inspector, a aba Terrain Settings. Nela, estão os parâmetros que a Unity nos permite configurar.

### São eles: ###

* Base Terrain 
* Tree & Detail Objects 
* Wind Settings for Grass 
* Mesh Resolution 
* Texture Resolutions

#### Manual detalhado em: /Docs/TerrainSettings.odt ####
