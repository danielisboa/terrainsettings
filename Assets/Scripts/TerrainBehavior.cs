﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainBehavior : MonoBehaviour
{
	// https://docs.unity3d.com/ScriptReference/Terrain.html  >>> Terrain | Unity Documentation

	public bool activeDetails; // liga e desliga detalhes do terreno aumentados

	public float basemapDist; // distância que as texturas mantém resolução padrão
	public float detailObjectDistance; // distância entre a câmera e os detalhes que serão cortados

	public float treeDistance; // distância entre a câmera e os detalhes que serão cortados das árvores
	public float treeCrossFadeLength; // distância que as árvores deixam de ser objetos 3D para serem imagens
	
	void Start ()
	{		
		activeDetails = true;
	}		

	void Update ()
	{
		if(activeDetails)
		{			
			Terrain.activeTerrain.detailObjectDistance = 2000;
			Terrain.activeTerrain.basemapDistance = 6000;
			Terrain.activeTerrain.treeDistance = 2000;
			Terrain.activeTerrain.treeCrossFadeLength = 2000;			
		}else{
			Terrain.activeTerrain.detailObjectDistance = 200;
			Terrain.activeTerrain.basemapDistance = 600;
			Terrain.activeTerrain.treeDistance = 200;
			Terrain.activeTerrain.treeCrossFadeLength = 200;
		}
	}
}